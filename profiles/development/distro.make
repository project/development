; Use this file to build a full distribution including Drupal core and the
; Drupal Developer install profile using the following command:
;
; drush make distro.make <target directory>

api = 2
core = 7.x

projects[drupal][type] = core
projects[drupal][version] = "7.41"

projects[drupal_developer][type] = "profile"
projects[drupal_developer][download][type] = "git"
projects[drupal_developer][download][url] = "git://github.com/verbruggenalex/development.git"
projects[drupal_developer][download][branch] = "master"
