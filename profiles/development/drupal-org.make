; development make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[admin_menu_environmental][version] = "1.x-dev"
projects[admin_menu_environmental][subdir] = "contrib"

projects[admin_views][version] = "1.5"
projects[admin_views][subdir] = "contrib"

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[coder][version] = "1.2"
projects[coder][subdir] = "contrib"

projects[devel][version] = "1.5"
projects[devel][subdir] = "contrib"

projects[devel_debug_log][version] = "1.2"
projects[devel_debug_log][subdir] = "contrib"

projects[devel_themer][version] = "1.x-dev"
projects[devel_themer][subdir] = "contrib"

projects[drupal_reset][version] = "1.4"
projects[drupal_reset][subdir] = "contrib"

projects[module_builder][version] = "2.x-dev"
projects[module_builder][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[search_krumo][version] = "1.6"
projects[search_krumo][subdir] = "contrib"

projects[features][version] = "2.7"
projects[features][subdir] = "contrib"

projects[simplehtmldom][version] = "1.12"
projects[simplehtmldom][subdir] = "contrib"

projects[entity][version] = "1.6"
projects[entity][subdir] = "contrib"

projects[jquery_colorpicker][version] = "1.2"
projects[jquery_colorpicker][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[masquerade][version] = "1.0-rc7"
projects[masquerade][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[jquery_update][version] = "3.0-alpha3"
projects[jquery_update][subdir] = "contrib"

projects[variable][version] = "2.5"
projects[variable][subdir] = "contrib"

projects[views][version] = "3.13"
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = "3.3"
projects[views_bulk_operations][subdir] = "contrib"

; +++++ Libraries +++++

; jQuery Colorpicker
libraries[colorpicker][directory_name] = "colorpicker"
libraries[colorpicker][type] = "library"
libraries[colorpicker][destination] = "libraries"
libraries[colorpicker][download][type] = "get"
libraries[colorpicker][download][url] = "http://www.eyecon.ro/colorpicker/colorpicker.zip"

; jQuery Colorpicker
libraries[simplehtmldom][directory_name] = "simplehtmldom"
libraries[simplehtmldom][type] = "library"
libraries[simplehtmldom][destination] = "libraries"
libraries[simplehtmldom][download][type] = "get"
libraries[simplehtmldom][download][url] = "http://sourceforge.net/projects/simplehtmldom/files/latest/download?source=files
