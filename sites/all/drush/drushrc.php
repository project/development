<?php 


$options['sites'] = array (
);
$options['profiles'] = array (
  0 => 'standard',
  1 => 'minimal',
  2 => 'development',
);
$options['packages'] = array (
  'base' => 
  array (
    'modules' => 
    array (
      'tracker' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/tracker/tracker.module',
        'basename' => 'tracker.module',
        'name' => 'tracker',
        'info' => 
        array (
          'name' => 'Tracker',
          'description' => 'Enables tracking of recent content for users.',
          'dependencies' => 
          array (
            0 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'tracker.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.41',
      ),
      'image' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/image/image.module',
        'basename' => 'image.module',
        'name' => 'image',
        'info' => 
        array (
          'name' => 'Image',
          'description' => 'Provides image manipulation tools.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'file',
          ),
          'files' => 
          array (
            0 => 'image.test',
          ),
          'configure' => 'admin/config/media/image-styles',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.41',
      ),
      'system' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/system/system.module',
        'basename' => 'system.module',
        'name' => 'system',
        'info' => 
        array (
          'name' => 'System',
          'description' => 'Handles general site configuration for administrators.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'system.archiver.inc',
            1 => 'system.mail.inc',
            2 => 'system.queue.inc',
            3 => 'system.tar.inc',
            4 => 'system.updater.inc',
            5 => 'system.test',
          ),
          'required' => true,
          'configure' => 'admin/config/system',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7080',
        'version' => '7.41',
      ),
      'forum' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/forum/forum.module',
        'basename' => 'forum.module',
        'name' => 'forum',
        'info' => 
        array (
          'name' => 'Forum',
          'description' => 'Provides discussion forums.',
          'dependencies' => 
          array (
            0 => 'taxonomy',
            1 => 'comment',
          ),
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'forum.test',
          ),
          'configure' => 'admin/structure/forum',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'forum.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7012',
        'version' => '7.41',
      ),
      'book' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/book/book.module',
        'basename' => 'book.module',
        'name' => 'book',
        'info' => 
        array (
          'name' => 'Book',
          'description' => 'Allows users to create and organize related content in an outline.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'book.test',
          ),
          'configure' => 'admin/content/book/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'book.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'help' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/help/help.module',
        'basename' => 'help.module',
        'name' => 'help',
        'info' => 
        array (
          'name' => 'Help',
          'description' => 'Manages the display of online help.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'help.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'shortcut' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/shortcut/shortcut.module',
        'basename' => 'shortcut.module',
        'name' => 'shortcut',
        'info' => 
        array (
          'name' => 'Shortcut',
          'description' => 'Allows users to manage customizable lists of shortcut links.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'shortcut.test',
          ),
          'configure' => 'admin/config/user-interface/shortcut',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'search' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/search/search.module',
        'basename' => 'search.module',
        'name' => 'search',
        'info' => 
        array (
          'name' => 'Search',
          'description' => 'Enables site-wide keyword searching.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'search.extender.inc',
            1 => 'search.test',
          ),
          'configure' => 'admin/config/search/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'search.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.41',
      ),
      'poll' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/poll/poll.module',
        'basename' => 'poll.module',
        'name' => 'poll',
        'info' => 
        array (
          'name' => 'Poll',
          'description' => 'Allows your site to capture votes on different topics in the form of multiple choice questions.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'poll.test',
          ),
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'poll.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.41',
      ),
      'dblog' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/dblog/dblog.module',
        'basename' => 'dblog.module',
        'name' => 'dblog',
        'info' => 
        array (
          'name' => 'Database logging',
          'description' => 'Logs and records system events to the database.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'dblog.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.41',
      ),
      'block' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/block/block.module',
        'basename' => 'block.module',
        'name' => 'block',
        'info' => 
        array (
          'name' => 'Block',
          'description' => 'Controls the visual building blocks a page is constructed with. Blocks are boxes of content rendered into an area, or region, of a web page.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'block.test',
          ),
          'configure' => 'admin/structure/block',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7009',
        'version' => '7.41',
      ),
      'translation' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/translation/translation.module',
        'basename' => 'translation.module',
        'name' => 'translation',
        'info' => 
        array (
          'name' => 'Content translation',
          'description' => 'Allows content to be translated into different languages.',
          'dependencies' => 
          array (
            0 => 'locale',
          ),
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'translation.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'color' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/color/color.module',
        'basename' => 'color.module',
        'name' => 'color',
        'info' => 
        array (
          'name' => 'Color',
          'description' => 'Allows administrators to change the color scheme of compatible themes.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'color.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.41',
      ),
      'syslog' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/syslog/syslog.module',
        'basename' => 'syslog.module',
        'name' => 'syslog',
        'info' => 
        array (
          'name' => 'Syslog',
          'description' => 'Logs and records system events to syslog.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'syslog.test',
          ),
          'configure' => 'admin/config/development/logging',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'path' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/path/path.module',
        'basename' => 'path.module',
        'name' => 'path',
        'info' => 
        array (
          'name' => 'Path',
          'description' => 'Allows users to rename URLs.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'path.test',
          ),
          'configure' => 'admin/config/search/path',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'locale' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/locale/locale.module',
        'basename' => 'locale.module',
        'name' => 'locale',
        'info' => 
        array (
          'name' => 'Locale',
          'description' => 'Adds language handling functionality and enables the translation of the user interface to languages other than English.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'locale.test',
          ),
          'configure' => 'admin/config/regional/language',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7005',
        'version' => '7.41',
      ),
      'file' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/file/file.module',
        'basename' => 'file.module',
        'name' => 'file',
        'info' => 
        array (
          'name' => 'File',
          'description' => 'Defines a file field type.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'tests/file.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'node' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/node/node.module',
        'basename' => 'node.module',
        'name' => 'node',
        'info' => 
        array (
          'name' => 'Node',
          'description' => 'Allows content to be submitted to the site and displayed on pages.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'node.module',
            1 => 'node.test',
          ),
          'required' => true,
          'configure' => 'admin/structure/types',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'node.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7015',
        'version' => '7.41',
      ),
      'overlay' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/overlay/overlay.module',
        'basename' => 'overlay.module',
        'name' => 'overlay',
        'info' => 
        array (
          'name' => 'Overlay',
          'description' => 'Displays the Drupal administration interface in an overlay.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'contextual' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/contextual/contextual.module',
        'basename' => 'contextual.module',
        'name' => 'contextual',
        'info' => 
        array (
          'name' => 'Contextual links',
          'description' => 'Provides contextual links to perform actions related to elements on a page.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contextual.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'menu' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/menu/menu.module',
        'basename' => 'menu.module',
        'name' => 'menu',
        'info' => 
        array (
          'name' => 'Menu',
          'description' => 'Allows administrators to customize the site navigation menu.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'menu.test',
          ),
          'configure' => 'admin/structure/menu',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.41',
      ),
      'contact' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/contact/contact.module',
        'basename' => 'contact.module',
        'name' => 'contact',
        'info' => 
        array (
          'name' => 'Contact',
          'description' => 'Enables the use of both personal and site-wide contact forms.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'contact.test',
          ),
          'configure' => 'admin/structure/contact',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.41',
      ),
      'field_sql_storage' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/field/modules/field_sql_storage/field_sql_storage.module',
        'basename' => 'field_sql_storage.module',
        'name' => 'field_sql_storage',
        'info' => 
        array (
          'name' => 'Field SQL storage',
          'description' => 'Stores field data in an SQL database.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_sql_storage.test',
          ),
          'required' => true,
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.41',
      ),
      'list' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/field/modules/list/list.module',
        'basename' => 'list.module',
        'name' => 'list',
        'info' => 
        array (
          'name' => 'List',
          'description' => 'Defines list field types. Use with Options to create selection lists.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
            1 => 'options',
          ),
          'files' => 
          array (
            0 => 'tests/list.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.41',
      ),
      'number' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/field/modules/number/number.module',
        'basename' => 'number.module',
        'name' => 'number',
        'info' => 
        array (
          'name' => 'Number',
          'description' => 'Defines numeric field types.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'number.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'options' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/field/modules/options/options.module',
        'basename' => 'options.module',
        'name' => 'options',
        'info' => 
        array (
          'name' => 'Options',
          'description' => 'Defines selection, check box and radio button widgets for text and numeric fields.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'options.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'text' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/field/modules/text/text.module',
        'basename' => 'text.module',
        'name' => 'text',
        'info' => 
        array (
          'name' => 'Text',
          'description' => 'Defines simple text field types.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'text.test',
          ),
          'required' => true,
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.41',
      ),
      'field' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/field/field.module',
        'basename' => 'field.module',
        'name' => 'field',
        'info' => 
        array (
          'name' => 'Field',
          'description' => 'Field API to add fields to entities like nodes and users.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'field.module',
            1 => 'field.attach.inc',
            2 => 'field.info.class.inc',
            3 => 'tests/field.test',
          ),
          'dependencies' => 
          array (
            0 => 'field_sql_storage',
          ),
          'required' => true,
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'theme/field.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7003',
        'version' => '7.41',
      ),
      'user' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/user/user.module',
        'basename' => 'user.module',
        'name' => 'user',
        'info' => 
        array (
          'name' => 'User',
          'description' => 'Manages the user registration and login system.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'user.module',
            1 => 'user.test',
          ),
          'required' => true,
          'configure' => 'admin/config/people',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'user.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7018',
        'version' => '7.41',
      ),
      'field_ui' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/field_ui/field_ui.module',
        'basename' => 'field_ui.module',
        'name' => 'field_ui',
        'info' => 
        array (
          'name' => 'Field UI',
          'description' => 'User interface for the Field API.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'field',
          ),
          'files' => 
          array (
            0 => 'field_ui.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'update' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/update/update.module',
        'basename' => 'update.module',
        'name' => 'update',
        'info' => 
        array (
          'name' => 'Update manager',
          'description' => 'Checks for available updates, and can securely install or update modules and themes via a web interface.',
          'version' => '7.41',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'update.test',
          ),
          'configure' => 'admin/reports/updates/settings',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7001',
        'version' => '7.41',
      ),
      'simpletest' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/simpletest/simpletest.module',
        'basename' => 'simpletest.module',
        'name' => 'simpletest',
        'info' => 
        array (
          'name' => 'Testing',
          'description' => 'Provides a framework for unit and functional testing.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'simpletest.test',
            1 => 'drupal_web_test_case.php',
            2 => 'tests/actions.test',
            3 => 'tests/ajax.test',
            4 => 'tests/batch.test',
            5 => 'tests/boot.test',
            6 => 'tests/bootstrap.test',
            7 => 'tests/cache.test',
            8 => 'tests/common.test',
            9 => 'tests/database_test.test',
            10 => 'tests/entity_crud.test',
            11 => 'tests/entity_crud_hook_test.test',
            12 => 'tests/entity_query.test',
            13 => 'tests/error.test',
            14 => 'tests/file.test',
            15 => 'tests/filetransfer.test',
            16 => 'tests/form.test',
            17 => 'tests/graph.test',
            18 => 'tests/image.test',
            19 => 'tests/lock.test',
            20 => 'tests/mail.test',
            21 => 'tests/menu.test',
            22 => 'tests/module.test',
            23 => 'tests/pager.test',
            24 => 'tests/password.test',
            25 => 'tests/path.test',
            26 => 'tests/registry.test',
            27 => 'tests/schema.test',
            28 => 'tests/session.test',
            29 => 'tests/tablesort.test',
            30 => 'tests/theme.test',
            31 => 'tests/unicode.test',
            32 => 'tests/update.test',
            33 => 'tests/xmlrpc.test',
            34 => 'tests/upgrade/upgrade.test',
            35 => 'tests/upgrade/upgrade.comment.test',
            36 => 'tests/upgrade/upgrade.filter.test',
            37 => 'tests/upgrade/upgrade.forum.test',
            38 => 'tests/upgrade/upgrade.locale.test',
            39 => 'tests/upgrade/upgrade.menu.test',
            40 => 'tests/upgrade/upgrade.node.test',
            41 => 'tests/upgrade/upgrade.taxonomy.test',
            42 => 'tests/upgrade/upgrade.trigger.test',
            43 => 'tests/upgrade/upgrade.translatable.test',
            44 => 'tests/upgrade/upgrade.upload.test',
            45 => 'tests/upgrade/upgrade.user.test',
            46 => 'tests/upgrade/update.aggregator.test',
            47 => 'tests/upgrade/update.trigger.test',
            48 => 'tests/upgrade/update.field.test',
            49 => 'tests/upgrade/update.user.test',
          ),
          'configure' => 'admin/config/development/testing/settings',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'toolbar' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/toolbar/toolbar.module',
        'basename' => 'toolbar.module',
        'name' => 'toolbar',
        'info' => 
        array (
          'name' => 'Toolbar',
          'description' => 'Provides a toolbar that shows the top-level administration menu items and links from other modules.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.41',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'blog' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/blog/blog.module',
        'basename' => 'blog.module',
        'name' => 'blog',
        'info' => 
        array (
          'name' => 'Blog',
          'description' => 'Enables multi-user blogs.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'blog.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'openid' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/openid/openid.module',
        'basename' => 'openid.module',
        'name' => 'openid',
        'info' => 
        array (
          'name' => 'OpenID',
          'description' => 'Allows users to log into your site using OpenID.',
          'version' => '7.41',
          'package' => 'Core',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'openid.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.41',
      ),
      'statistics' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/statistics/statistics.module',
        'basename' => 'statistics.module',
        'name' => 'statistics',
        'info' => 
        array (
          'name' => 'Statistics',
          'description' => 'Logs access statistics for your site.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'statistics.test',
          ),
          'configure' => 'admin/config/system/statistics',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7000',
        'version' => '7.41',
      ),
      'filter' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/filter/filter.module',
        'basename' => 'filter.module',
        'name' => 'filter',
        'info' => 
        array (
          'name' => 'Filter',
          'description' => 'Filters content in preparation for display.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'filter.test',
          ),
          'required' => true,
          'configure' => 'admin/config/content/formats',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7010',
        'version' => '7.41',
      ),
      'dashboard' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/dashboard/dashboard.module',
        'basename' => 'dashboard.module',
        'name' => 'dashboard',
        'info' => 
        array (
          'name' => 'Dashboard',
          'description' => 'Provides a dashboard page in the administrative interface for organizing administrative tasks and tracking information within your site.',
          'core' => '7.x',
          'package' => 'Core',
          'version' => '7.41',
          'files' => 
          array (
            0 => 'dashboard.test',
          ),
          'dependencies' => 
          array (
            0 => 'block',
          ),
          'configure' => 'admin/dashboard/customize',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'rdf' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/rdf/rdf.module',
        'basename' => 'rdf.module',
        'name' => 'rdf',
        'info' => 
        array (
          'name' => 'RDF',
          'description' => 'Enriches your content with metadata to let other applications (e.g. search engines, aggregators) better understand its relationships and attributes.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'rdf.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'php' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/php/php.module',
        'basename' => 'php.module',
        'name' => 'php',
        'info' => 
        array (
          'name' => 'PHP filter',
          'description' => 'Allows embedded PHP code/snippets to be evaluated.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'php.test',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => 0,
        'version' => '7.41',
      ),
      'trigger' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/trigger/trigger.module',
        'basename' => 'trigger.module',
        'name' => 'trigger',
        'info' => 
        array (
          'name' => 'Trigger',
          'description' => 'Enables actions to be fired on certain system events, such as when new content is created.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'trigger.test',
          ),
          'configure' => 'admin/structure/trigger',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7002',
        'version' => '7.41',
      ),
      'comment' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/comment/comment.module',
        'basename' => 'comment.module',
        'name' => 'comment',
        'info' => 
        array (
          'name' => 'Comment',
          'description' => 'Allows users to comment on and discuss published content.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'text',
          ),
          'files' => 
          array (
            0 => 'comment.module',
            1 => 'comment.test',
          ),
          'configure' => 'admin/content/comment',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'comment.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7009',
        'version' => '7.41',
      ),
      'aggregator' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/aggregator/aggregator.module',
        'basename' => 'aggregator.module',
        'name' => 'aggregator',
        'info' => 
        array (
          'name' => 'Aggregator',
          'description' => 'Aggregates syndicated content (RSS, RDF, and Atom feeds).',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'files' => 
          array (
            0 => 'aggregator.test',
          ),
          'configure' => 'admin/config/services/aggregator/settings',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'aggregator.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'dependencies' => 
          array (
          ),
          'php' => '5.2.4',
        ),
        'schema_version' => '7004',
        'version' => '7.41',
      ),
      'taxonomy' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/modules/taxonomy/taxonomy.module',
        'basename' => 'taxonomy.module',
        'name' => 'taxonomy',
        'info' => 
        array (
          'name' => 'Taxonomy',
          'description' => 'Enables the categorization of content.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'options',
          ),
          'files' => 
          array (
            0 => 'taxonomy.module',
            1 => 'taxonomy.test',
          ),
          'configure' => 'admin/structure/taxonomy',
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
        ),
        'schema_version' => '7011',
        'version' => '7.41',
      ),
    ),
    'themes' => 
    array (
      'seven' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/themes/seven/seven.info',
        'basename' => 'seven.info',
        'name' => 'Seven',
        'info' => 
        array (
          'name' => 'Seven',
          'description' => 'A simple one-column, tableless, fluid width administration theme.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'screen' => 
            array (
              0 => 'reset.css',
              1 => 'style.css',
            ),
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '1',
          ),
          'regions' => 
          array (
            'content' => 'Content',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'sidebar_first' => 'First sidebar',
          ),
          'regions_hidden' => 
          array (
            0 => 'sidebar_first',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
        ),
        'version' => '7.41',
      ),
      'bartik' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/themes/bartik/bartik.info',
        'basename' => 'bartik.info',
        'name' => 'Bartik',
        'info' => 
        array (
          'name' => 'Bartik',
          'description' => 'A flexible, recolorable theme with many regions.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'css/layout.css',
              1 => 'css/style.css',
              2 => 'css/colors.css',
            ),
            'print' => 
            array (
              0 => 'css/print.css',
            ),
          ),
          'regions' => 
          array (
            'header' => 'Header',
            'help' => 'Help',
            'page_top' => 'Page top',
            'page_bottom' => 'Page bottom',
            'highlighted' => 'Highlighted',
            'featured' => 'Featured',
            'content' => 'Content',
            'sidebar_first' => 'Sidebar first',
            'sidebar_second' => 'Sidebar second',
            'triptych_first' => 'Triptych first',
            'triptych_middle' => 'Triptych middle',
            'triptych_last' => 'Triptych last',
            'footer_firstcolumn' => 'Footer first column',
            'footer_secondcolumn' => 'Footer second column',
            'footer_thirdcolumn' => 'Footer third column',
            'footer_fourthcolumn' => 'Footer fourth column',
            'footer' => 'Footer',
          ),
          'settings' => 
          array (
            'shortcut_module_link' => '0',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
        ),
        'version' => '7.41',
      ),
      'stark' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/themes/stark/stark.info',
        'basename' => 'stark.info',
        'name' => 'Stark',
        'info' => 
        array (
          'name' => 'Stark',
          'description' => 'This theme demonstrates Drupal\'s default HTML markup and CSS styles. To learn how to build your own theme and override Drupal\'s default code, see the <a href="http://drupal.org/theme-guide">Theming Guide</a>.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'layout.css',
            ),
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
        ),
        'version' => '7.41',
      ),
      'garland' => 
      array (
        'filename' => '/var/aegir/platforms/development-7.41/themes/garland/garland.info',
        'basename' => 'garland.info',
        'name' => 'Garland',
        'info' => 
        array (
          'name' => 'Garland',
          'description' => 'A multi-column theme which can be configured to modify colors and switch between fixed and fluid width layouts.',
          'package' => 'Core',
          'version' => '7.41',
          'core' => '7.x',
          'stylesheets' => 
          array (
            'all' => 
            array (
              0 => 'style.css',
            ),
            'print' => 
            array (
              0 => 'print.css',
            ),
          ),
          'settings' => 
          array (
            'garland_width' => 'fluid',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
        ),
        'version' => '7.41',
      ),
    ),
    'platforms' => 
    array (
      'drupal' => 
      array (
        'short_name' => 'drupal',
        'version' => '7.41',
        'description' => 'This platform is running Drupal 7.41',
      ),
    ),
    'profiles' => 
    array (
      'standard' => 
      array (
        'name' => 'standard',
        'filename' => './profiles/standard/standard.profile',
        'info' => 
        array (
          'name' => 'Standard',
          'description' => 'Install with commonly used features pre-configured.',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'color',
            2 => 'comment',
            3 => 'contextual',
            4 => 'dashboard',
            5 => 'help',
            6 => 'image',
            7 => 'list',
            8 => 'menu',
            9 => 'number',
            10 => 'options',
            11 => 'path',
            12 => 'taxonomy',
            13 => 'dblog',
            14 => 'search',
            15 => 'shortcut',
            16 => 'toolbar',
            17 => 'overlay',
            18 => 'field_ui',
            19 => 'file',
            20 => 'rdf',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
          'old_short_name' => 'default',
        ),
        'version' => '7.41',
      ),
      'minimal' => 
      array (
        'name' => 'minimal',
        'filename' => './profiles/minimal/minimal.profile',
        'info' => 
        array (
          'name' => 'Minimal',
          'description' => 'Start with only a few modules enabled.',
          'version' => '7.41',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'block',
            1 => 'dblog',
          ),
          'project' => 'drupal',
          'datestamp' => '1445457729',
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
        ),
        'version' => '7.41',
      ),
      'development' => 
      array (
        'name' => 'development',
        'filename' => './profiles/development/development.profile',
        'info' => 
        array (
          'name' => 'Development',
          'description' => 'Drupal install profile with admin and devel modules enabled.',
          'core' => '7.x',
          'dependencies' => 
          array (
            0 => 'admin_menu',
            1 => 'admin_menu_environmental',
            2 => 'admin_menu_toolbar',
            3 => 'admin_views',
            4 => 'module_filter',
            5 => 'ctools',
            6 => 'block',
            7 => 'dblog',
            8 => 'field',
            9 => 'field_sql_storage',
            10 => 'filter',
            11 => 'node',
            12 => 'system',
            13 => 'text',
            14 => 'update',
            15 => 'user',
            16 => 'coder',
            17 => 'coder_review',
            18 => 'devel',
            19 => 'devel_debug_log',
            20 => 'devel_generate',
            21 => 'devel_themer',
            22 => 'drupal_reset',
            23 => 'module_builder',
            24 => 'profiler_builder',
            25 => 'profiler_builder_extras',
            26 => 'search_krumo',
            27 => 'simplehtmldom',
            28 => 'entity',
            29 => 'entity_token',
            30 => 'jquery_colorpicker',
            31 => 'libraries',
            32 => 'masquerade',
            33 => 'strongarm',
            34 => 'token',
            35 => 'jquery_update',
            36 => 'variable',
            37 => 'views',
            38 => 'views_bulk_operations',
            39 => 'views_ui',
            40 => 'features',
          ),
          'files' => 
          array (
            0 => 'drupal_developer.profile',
          ),
          'version' => NULL,
          'php' => '5.2.4',
          'languages' => 
          array (
            0 => 'en',
          ),
        ),
        'version' => '7.41',
      ),
    ),
  ),
  'sites-all' => 
  array (
    'modules' => 
    array (
    ),
    'themes' => 
    array (
    ),
  ),
  'profiles' => 
  array (
    'standard' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
    'minimal' => 
    array (
      'modules' => 
      array (
      ),
      'themes' => 
      array (
      ),
    ),
    'development' => 
    array (
      'modules' => 
      array (
        'devel_generate' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/devel/devel_generate/devel_generate.module',
          'basename' => 'devel_generate.module',
          'name' => 'devel_generate',
          'info' => 
          array (
            'name' => 'Devel generate',
            'description' => 'Generate dummy users, nodes, and taxonomy terms.',
            'package' => 'Development',
            'core' => '7.x',
            'tags' => 
            array (
              0 => 'developer',
            ),
            'configure' => 'admin/config/development/generate',
            'files' => 
            array (
              0 => 'devel_generate.test',
            ),
            'version' => '7.x-1.5',
            'project' => 'devel',
            'datestamp' => '1398963366',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.5',
        ),
        'devel_node_access' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/devel/devel_node_access.module',
          'basename' => 'devel_node_access.module',
          'name' => 'devel_node_access',
          'info' => 
          array (
            'name' => 'Devel node access',
            'description' => 'Developer blocks and page illustrating relevant node_access records.',
            'package' => 'Development',
            'dependencies' => 
            array (
              0 => 'menu',
            ),
            'core' => '7.x',
            'configure' => 'admin/config/development/devel',
            'tags' => 
            array (
              0 => 'developer',
            ),
            'version' => '7.x-1.5',
            'project' => 'devel',
            'datestamp' => '1398963366',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.5',
        ),
        'devel' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/devel/devel.module',
          'basename' => 'devel.module',
          'name' => 'devel',
          'info' => 
          array (
            'name' => 'Devel',
            'description' => 'Various blocks, pages, and functions for developers.',
            'package' => 'Development',
            'core' => '7.x',
            'configure' => 'admin/config/development/devel',
            'tags' => 
            array (
              0 => 'developer',
            ),
            'files' => 
            array (
              0 => 'devel.test',
              1 => 'devel.mail.inc',
            ),
            'version' => '7.x-1.5',
            'project' => 'devel',
            'datestamp' => '1398963366',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7006',
          'version' => '7.x-1.5',
        ),
        'simplehtmldom' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/simplehtmldom/simplehtmldom.module',
          'basename' => 'simplehtmldom.module',
          'name' => 'simplehtmldom',
          'info' => 
          array (
            'name' => 'simplehtmldom API',
            'description' => 'A wrapper module around the simplehtmldom PHP library at sourceforge.',
            'package' => 'Filter',
            'core' => '7.x',
            'version' => '7.x-1.12',
            'project' => 'simplehtmldom',
            'datestamp' => '1307968619',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.12',
        ),
        'styleguide_palette' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/styleguide/styleguide_palette/styleguide_palette.module',
          'basename' => 'styleguide_palette.module',
          'name' => 'styleguide_palette',
          'info' => 
          array (
            'name' => 'Style guide palette',
            'description' => 'Stores and displays color palettes.',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'styleguide',
            ),
            'configure' => 'admin/config/user-interface/styleguide-palette',
            'files' => 
            array (
              0 => 'styleguide_palette.test',
            ),
            'version' => '7.x-1.1',
            'project' => 'styleguide',
            'datestamp' => '1367467219',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.1',
        ),
        'styleguide' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/styleguide/styleguide.module',
          'basename' => 'styleguide.module',
          'name' => 'styleguide',
          'info' => 
          array (
            'name' => 'Style guide',
            'description' => 'Generates a theme style guide for proofing common elements.',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'styleguide.module',
              1 => 'styleguide.styleguide.inc',
            ),
            'version' => '7.x-1.1',
            'project' => 'styleguide',
            'datestamp' => '1367467219',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.1',
        ),
        'module_builder' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/module_builder/module_builder.module',
          'basename' => 'module_builder.module',
          'name' => 'module_builder',
          'info' => 
          array (
            'name' => 'Module Builder',
            'description' => 'Builds scaffolding for custom modules.',
            'package' => 'Development',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'module_builder.module',
              1 => 'includes/module_builder.admin.inc',
              2 => 'tests/module_builder.test',
            ),
            'version' => '7.x-2.x-dev',
            'project' => 'module_builder',
            'datestamp' => '1448138653',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.x-dev',
        ),
        'devel_debug_log' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/devel_debug_log/devel_debug_log.module',
          'basename' => 'devel_debug_log.module',
          'name' => 'devel_debug_log',
          'info' => 
          array (
            'name' => 'Devel debug log',
            'description' => 'Creates a page that shows debug messages.',
            'package' => 'Development',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'devel',
            ),
            'version' => '7.x-1.2',
            'project' => 'devel_debug_log',
            'datestamp' => '1401723828',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.2',
        ),
        'drupal_reset' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/drupal_reset/drupal_reset.module',
          'basename' => 'drupal_reset.module',
          'name' => 'drupal_reset',
          'info' => 
          array (
            'name' => 'Drupal Reset',
            'description' => 'Resets the current site by doing one or both of deleting the site files directory, and deleting all database tables and redirecting to install.php.',
            'core' => '7.x',
            'package' => 'Development',
            'configure' => 'admin/config/development/drupal_reset',
            'files' => 
            array (
              0 => 'drupal_reset.lib.inc',
            ),
            'version' => '7.x-1.4',
            'project' => 'drupal_reset',
            'datestamp' => '1406563134',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.4',
        ),
        'profiler_builder_extras' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/profiler_builder/modules/profiler_builder_extras/profiler_builder_extras.module',
          'basename' => 'profiler_builder_extras.module',
          'name' => 'profiler_builder_extras',
          'info' => 
          array (
            'name' => 'Profiler Builder Extras',
            'description' => 'Extra tools for use with profiler builder, more experimental.',
            'package' => 'Development',
            'core' => '7.x',
            'configure' => 'admin/config/development/profiler_builder/extras',
            'version' => '7.x-1.2',
            'project' => 'profiler_builder',
            'datestamp' => '1409079116',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.2',
        ),
        'profiler_builder' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/profiler_builder/profiler_builder.module',
          'basename' => 'profiler_builder.module',
          'name' => 'profiler_builder',
          'info' => 
          array (
            'name' => 'Profiler Builder',
            'description' => 'Turn this site into a profiler distribution fast!',
            'package' => 'Development',
            'core' => '7.x',
            'configure' => 'admin/config/development/profiler_builder',
            'version' => '7.x-1.2',
            'project' => 'profiler_builder',
            'datestamp' => '1409079116',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.2',
        ),
        'search_krumo' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/search_krumo/search_krumo.module',
          'basename' => 'search_krumo.module',
          'name' => 'search_krumo',
          'info' => 
          array (
            'name' => 'Search Krumo',
            'description' => 'Makes it possible to search through Devels krumo\'s.',
            'core' => '7.x',
            'stylesheets' => 
            array (
              'all' => 
              array (
                0 => 'search_krumo.css',
              ),
            ),
            'package' => 'Development',
            'dependencies' => 
            array (
              0 => 'devel',
            ),
            'version' => '7.x-1.6',
            'project' => 'search_krumo',
            'datestamp' => '1429278782',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.6',
        ),
        'devel_themer' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/devel_themer/devel_themer.module',
          'basename' => 'devel_themer.module',
          'name' => 'devel_themer',
          'info' => 
          array (
            'name' => 'Theme developer',
            'description' => 'Essential theme API information for theme developers',
            'package' => 'Development',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'devel',
              1 => 'simplehtmldom (1.x)',
            ),
            'configure' => 'admin/config/development/devel_themer',
            'files' => 
            array (
              0 => 'devel_themer.module',
            ),
            'version' => '7.x-1.x-dev',
            'project' => 'devel_themer',
            'datestamp' => '1416866288',
            'php' => '5.2.4',
          ),
          'schema_version' => '6001',
          'version' => '7.x-1.x-dev',
        ),
        'example' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/coder/coder_upgrade/tests/new/samples/example.module',
          'basename' => 'example.module',
          'name' => 'example',
          'info' => 
          array (
            'dependencies' => 
            array (
            ),
            'description' => '',
            'version' => NULL,
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => NULL,
        ),
        'coder_upgrade' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/coder/coder_upgrade/coder_upgrade.module',
          'basename' => 'coder_upgrade.module',
          'name' => 'coder_upgrade',
          'info' => 
          array (
            'name' => 'Coder Upgrade',
            'description' => 'Modifies source code to assist with the upgrade of a module for changes to a relied upon API.',
            'package' => 'Development',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'gplib (<2.0)',
            ),
            'files' => 
            array (
              0 => 'coder_upgrade.test',
            ),
            'configure' => 'admin/config/development/coder/upgrade/settings',
            'version' => '7.x-1.2',
            'project' => 'coder',
            'datestamp' => '1352602857',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.2',
        ),
        'coder_review' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/coder/coder_review/coder_review.module',
          'basename' => 'coder_review.module',
          'name' => 'coder_review',
          'info' => 
          array (
            'name' => 'Coder Review',
            'description' => 'Developer module which reviews your code identifying coding style problems and where updates to the API are required.',
            'package' => 'Development',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'coder',
            ),
            'files' => 
            array (
              0 => 'tests/coder_review_test_case.tinc',
              1 => 'tests/coder_review_6x.test',
              2 => 'tests/coder_review_7x.test',
              3 => 'tests/coder_review_comment.test',
              4 => 'tests/coder_review_i18n.test',
              5 => 'tests/coder_review_security.test',
              6 => 'tests/coder_review_sql.test',
              7 => 'tests/coder_review_style.test',
            ),
            'version' => '7.x-1.2',
            'project' => 'coder',
            'datestamp' => '1352602857',
            'php' => '5.2.4',
          ),
          'schema_version' => '3',
          'version' => '7.x-1.2',
        ),
        'coder' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/devel/coder/coder.module',
          'basename' => 'coder.module',
          'name' => 'coder',
          'info' => 
          array (
            'name' => 'Coder',
            'description' => 'Developer Module that assists with code review and version upgrade',
            'package' => 'Development',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'coder.module',
            ),
            'version' => '7.x-1.2',
            'project' => 'coder',
            'datestamp' => '1352602857',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.2',
        ),
        'pathauto_entity' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/pathauto_entity/pathauto_entity.module',
          'basename' => 'pathauto_entity.module',
          'name' => 'pathauto_entity',
          'info' => 
          array (
            'name' => 'Pathauto Entity',
            'description' => 'Custom entity type support for Pathauto module.',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'pathauto (>=7.x-1.2)',
              1 => 'entity_token',
            ),
            'version' => '7.x-1.0',
            'project' => 'pathauto_entity',
            'datestamp' => '1407273228',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.0',
        ),
        'strongarm' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/strongarm/strongarm.module',
          'basename' => 'strongarm.module',
          'name' => 'strongarm',
          'info' => 
          array (
            'name' => 'Strongarm',
            'description' => 'Enforces variable values defined by modules that need settings set to operate properly.',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'files' => 
            array (
              0 => 'strongarm.admin.inc',
              1 => 'strongarm.install',
              2 => 'strongarm.module',
            ),
            'version' => '7.x-2.0',
            'project' => 'strongarm',
            'datestamp' => '1339604214',
            'php' => '5.2.4',
          ),
          'schema_version' => '7201',
          'version' => '7.x-2.0',
        ),
        'variable_views' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/variable/variable_views/variable_views.module',
          'basename' => 'variable_views.module',
          'name' => 'variable_views',
          'info' => 
          array (
            'name' => 'Variable views',
            'description' => 'Provides views integration for variable, included a default variable argument.',
            'dependencies' => 
            array (
              0 => 'variable',
              1 => 'views',
            ),
            'package' => 'Variable',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'includes/views_plugin_argument_default_variable.inc',
              1 => 'includes/views_handler_field_variable_title.inc',
              2 => 'includes/views_handler_field_variable_value.inc',
            ),
            'version' => '7.x-2.5',
            'project' => 'variable',
            'datestamp' => '1398250128',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.5',
        ),
        'variable_store' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/variable/variable_store/variable_store.module',
          'basename' => 'variable_store.module',
          'name' => 'variable_store',
          'info' => 
          array (
            'name' => 'Variable store',
            'description' => 'Database storage for variable realms. This is an API module.',
            'dependencies' => 
            array (
              0 => 'variable',
            ),
            'package' => 'Variable',
            'core' => '7.x',
            'version' => '7.x-2.5',
            'files' => 
            array (
              0 => 'variable_store.class.inc',
              1 => 'variable_store.test',
            ),
            'project' => 'variable',
            'datestamp' => '1398250128',
            'php' => '5.2.4',
          ),
          'schema_version' => '7000',
          'version' => '7.x-2.5',
        ),
        'variable_realm' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/variable/variable_realm/variable_realm.module',
          'basename' => 'variable_realm.module',
          'name' => 'variable_realm',
          'info' => 
          array (
            'name' => 'Variable realm',
            'description' => 'API to use variable realms from different modules',
            'dependencies' => 
            array (
              0 => 'variable',
            ),
            'package' => 'Variable',
            'core' => '7.x',
            'version' => '7.x-2.5',
            'files' => 
            array (
              0 => 'variable_realm.class.inc',
              1 => 'variable_realm_union.class.inc',
            ),
            'project' => 'variable',
            'datestamp' => '1398250128',
            'php' => '5.2.4',
          ),
          'schema_version' => '7000',
          'version' => '7.x-2.5',
        ),
        'variable_admin' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/variable/variable_admin/variable_admin.module',
          'basename' => 'variable_admin.module',
          'name' => 'variable_admin',
          'info' => 
          array (
            'name' => 'Variable admin',
            'description' => 'Variable Administration UI',
            'dependencies' => 
            array (
              0 => 'variable',
            ),
            'package' => 'Variable',
            'core' => '7.x',
            'version' => '7.x-2.5',
            'project' => 'variable',
            'datestamp' => '1398250128',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.5',
        ),
        'variable_example' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/variable/variable_example/variable_example.module',
          'basename' => 'variable_example.module',
          'name' => 'variable_example',
          'info' => 
          array (
            'name' => 'Variable example',
            'description' => 'An example module showing how to use the Variable API and providing some variables.',
            'dependencies' => 
            array (
              0 => 'variable',
              1 => 'variable_store',
            ),
            'package' => 'Example modules',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'variable_example.variable.inc',
            ),
            'version' => '7.x-2.5',
            'project' => 'variable',
            'datestamp' => '1398250128',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.5',
        ),
        'variable' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/variable/variable.module',
          'basename' => 'variable.module',
          'name' => 'variable',
          'info' => 
          array (
            'name' => 'Variable',
            'description' => 'Variable Information and basic variable API',
            'package' => 'Variable',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'includes/forum.variable.inc',
              1 => 'includes/locale.variable.inc',
              2 => 'includes/menu.variable.inc',
              3 => 'includes/node.variable.inc',
              4 => 'includes/system.variable.inc',
              5 => 'includes/taxonomy.variable.inc',
              6 => 'includes/translation.variable.inc',
              7 => 'includes/user.variable.inc',
              8 => 'variable.test',
            ),
            'version' => '7.x-2.5',
            'project' => 'variable',
            'datestamp' => '1398250128',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.5',
        ),
        'token' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/token/token.module',
          'basename' => 'token.module',
          'name' => 'token',
          'info' => 
          array (
            'name' => 'Token',
            'description' => 'Provides a user interface for the Token API and some missing core tokens.',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'token.test',
            ),
            'version' => '7.x-1.6',
            'project' => 'token',
            'datestamp' => '1425149060',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7001',
          'version' => '7.x-1.6',
        ),
        'entity' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/entity/entity.module',
          'basename' => 'entity.module',
          'name' => 'entity',
          'info' => 
          array (
            'name' => 'Entity API',
            'description' => 'Enables modules to work with any entity type and to provide entities.',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'entity.features.inc',
              1 => 'entity.i18n.inc',
              2 => 'entity.info.inc',
              3 => 'entity.rules.inc',
              4 => 'entity.test',
              5 => 'includes/entity.inc',
              6 => 'includes/entity.controller.inc',
              7 => 'includes/entity.ui.inc',
              8 => 'includes/entity.wrapper.inc',
              9 => 'views/entity.views.inc',
              10 => 'views/handlers/entity_views_field_handler_helper.inc',
              11 => 'views/handlers/entity_views_handler_area_entity.inc',
              12 => 'views/handlers/entity_views_handler_field_boolean.inc',
              13 => 'views/handlers/entity_views_handler_field_date.inc',
              14 => 'views/handlers/entity_views_handler_field_duration.inc',
              15 => 'views/handlers/entity_views_handler_field_entity.inc',
              16 => 'views/handlers/entity_views_handler_field_field.inc',
              17 => 'views/handlers/entity_views_handler_field_numeric.inc',
              18 => 'views/handlers/entity_views_handler_field_options.inc',
              19 => 'views/handlers/entity_views_handler_field_text.inc',
              20 => 'views/handlers/entity_views_handler_field_uri.inc',
              21 => 'views/handlers/entity_views_handler_relationship_by_bundle.inc',
              22 => 'views/handlers/entity_views_handler_relationship.inc',
              23 => 'views/plugins/entity_views_plugin_row_entity_view.inc',
            ),
            'version' => '7.x-1.6',
            'project' => 'entity',
            'datestamp' => '1424876582',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7003',
          'version' => '7.x-1.6',
        ),
        'entity_token' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/entity/entity_token.module',
          'basename' => 'entity_token.module',
          'name' => 'entity_token',
          'info' => 
          array (
            'name' => 'Entity tokens',
            'description' => 'Provides token replacements for all properties that have no tokens and are known to the entity API.',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'entity_token.tokens.inc',
              1 => 'entity_token.module',
            ),
            'dependencies' => 
            array (
              0 => 'entity',
            ),
            'version' => '7.x-1.6',
            'project' => 'entity',
            'datestamp' => '1424876582',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.6',
        ),
        'libraries' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/libraries/libraries.module',
          'basename' => 'libraries.module',
          'name' => 'libraries',
          'info' => 
          array (
            'name' => 'Libraries',
            'description' => 'Allows version-dependent and shared usage of external libraries.',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'system (>=7.11)',
            ),
            'files' => 
            array (
              0 => 'tests/libraries.test',
            ),
            'version' => '7.x-2.2',
            'project' => 'libraries',
            'datestamp' => '1391965716',
            'php' => '5.2.4',
          ),
          'schema_version' => '7200',
          'version' => '7.x-2.2',
        ),
        'views_export' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/views/views_export/views_export.module',
          'basename' => 'views_export.module',
          'name' => 'views_export',
          'info' => 
          array (
            'dependencies' => 
            array (
            ),
            'description' => '',
            'version' => NULL,
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => NULL,
        ),
        'views_ui' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/views/views_ui.module',
          'basename' => 'views_ui.module',
          'name' => 'views_ui',
          'info' => 
          array (
            'name' => 'Views UI',
            'description' => 'Administrative interface to views. Without this module, you cannot create or edit your views.',
            'package' => 'Views',
            'core' => '7.x',
            'configure' => 'admin/structure/views',
            'dependencies' => 
            array (
              0 => 'views',
            ),
            'files' => 
            array (
              0 => 'views_ui.module',
              1 => 'plugins/views_wizard/views_ui_base_views_wizard.class.php',
            ),
            'version' => '7.x-3.13',
            'project' => 'views',
            'datestamp' => '1446804876',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-3.13',
        ),
        'views' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/views/views.module',
          'basename' => 'views.module',
          'name' => 'views',
          'info' => 
          array (
            'name' => 'Views',
            'description' => 'Create customized lists and queries from your database.',
            'package' => 'Views',
            'core' => '7.x',
            'php' => '5.2',
            'stylesheets' => 
            array (
              'all' => 
              array (
                0 => 'css/views.css',
              ),
            ),
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'files' => 
            array (
              0 => 'handlers/views_handler_area.inc',
              1 => 'handlers/views_handler_area_messages.inc',
              2 => 'handlers/views_handler_area_result.inc',
              3 => 'handlers/views_handler_area_text.inc',
              4 => 'handlers/views_handler_area_text_custom.inc',
              5 => 'handlers/views_handler_area_view.inc',
              6 => 'handlers/views_handler_argument.inc',
              7 => 'handlers/views_handler_argument_date.inc',
              8 => 'handlers/views_handler_argument_formula.inc',
              9 => 'handlers/views_handler_argument_many_to_one.inc',
              10 => 'handlers/views_handler_argument_null.inc',
              11 => 'handlers/views_handler_argument_numeric.inc',
              12 => 'handlers/views_handler_argument_string.inc',
              13 => 'handlers/views_handler_argument_group_by_numeric.inc',
              14 => 'handlers/views_handler_field.inc',
              15 => 'handlers/views_handler_field_counter.inc',
              16 => 'handlers/views_handler_field_boolean.inc',
              17 => 'handlers/views_handler_field_contextual_links.inc',
              18 => 'handlers/views_handler_field_custom.inc',
              19 => 'handlers/views_handler_field_date.inc',
              20 => 'handlers/views_handler_field_entity.inc',
              21 => 'handlers/views_handler_field_markup.inc',
              22 => 'handlers/views_handler_field_math.inc',
              23 => 'handlers/views_handler_field_numeric.inc',
              24 => 'handlers/views_handler_field_prerender_list.inc',
              25 => 'handlers/views_handler_field_time_interval.inc',
              26 => 'handlers/views_handler_field_serialized.inc',
              27 => 'handlers/views_handler_field_machine_name.inc',
              28 => 'handlers/views_handler_field_url.inc',
              29 => 'handlers/views_handler_filter.inc',
              30 => 'handlers/views_handler_filter_boolean_operator.inc',
              31 => 'handlers/views_handler_filter_boolean_operator_string.inc',
              32 => 'handlers/views_handler_filter_combine.inc',
              33 => 'handlers/views_handler_filter_date.inc',
              34 => 'handlers/views_handler_filter_equality.inc',
              35 => 'handlers/views_handler_filter_entity_bundle.inc',
              36 => 'handlers/views_handler_filter_group_by_numeric.inc',
              37 => 'handlers/views_handler_filter_in_operator.inc',
              38 => 'handlers/views_handler_filter_many_to_one.inc',
              39 => 'handlers/views_handler_filter_numeric.inc',
              40 => 'handlers/views_handler_filter_string.inc',
              41 => 'handlers/views_handler_filter_fields_compare.inc',
              42 => 'handlers/views_handler_relationship.inc',
              43 => 'handlers/views_handler_relationship_groupwise_max.inc',
              44 => 'handlers/views_handler_sort.inc',
              45 => 'handlers/views_handler_sort_date.inc',
              46 => 'handlers/views_handler_sort_formula.inc',
              47 => 'handlers/views_handler_sort_group_by_numeric.inc',
              48 => 'handlers/views_handler_sort_menu_hierarchy.inc',
              49 => 'handlers/views_handler_sort_random.inc',
              50 => 'includes/base.inc',
              51 => 'includes/handlers.inc',
              52 => 'includes/plugins.inc',
              53 => 'includes/view.inc',
              54 => 'modules/aggregator/views_handler_argument_aggregator_fid.inc',
              55 => 'modules/aggregator/views_handler_argument_aggregator_iid.inc',
              56 => 'modules/aggregator/views_handler_argument_aggregator_category_cid.inc',
              57 => 'modules/aggregator/views_handler_field_aggregator_title_link.inc',
              58 => 'modules/aggregator/views_handler_field_aggregator_category.inc',
              59 => 'modules/aggregator/views_handler_field_aggregator_item_description.inc',
              60 => 'modules/aggregator/views_handler_field_aggregator_xss.inc',
              61 => 'modules/aggregator/views_handler_filter_aggregator_category_cid.inc',
              62 => 'modules/aggregator/views_plugin_row_aggregator_rss.inc',
              63 => 'modules/book/views_plugin_argument_default_book_root.inc',
              64 => 'modules/comment/views_handler_argument_comment_user_uid.inc',
              65 => 'modules/comment/views_handler_field_comment.inc',
              66 => 'modules/comment/views_handler_field_comment_depth.inc',
              67 => 'modules/comment/views_handler_field_comment_link.inc',
              68 => 'modules/comment/views_handler_field_comment_link_approve.inc',
              69 => 'modules/comment/views_handler_field_comment_link_delete.inc',
              70 => 'modules/comment/views_handler_field_comment_link_edit.inc',
              71 => 'modules/comment/views_handler_field_comment_link_reply.inc',
              72 => 'modules/comment/views_handler_field_comment_node_link.inc',
              73 => 'modules/comment/views_handler_field_comment_username.inc',
              74 => 'modules/comment/views_handler_field_ncs_last_comment_name.inc',
              75 => 'modules/comment/views_handler_field_ncs_last_updated.inc',
              76 => 'modules/comment/views_handler_field_node_comment.inc',
              77 => 'modules/comment/views_handler_field_node_new_comments.inc',
              78 => 'modules/comment/views_handler_field_last_comment_timestamp.inc',
              79 => 'modules/comment/views_handler_filter_comment_user_uid.inc',
              80 => 'modules/comment/views_handler_filter_ncs_last_updated.inc',
              81 => 'modules/comment/views_handler_filter_node_comment.inc',
              82 => 'modules/comment/views_handler_sort_comment_thread.inc',
              83 => 'modules/comment/views_handler_sort_ncs_last_comment_name.inc',
              84 => 'modules/comment/views_handler_sort_ncs_last_updated.inc',
              85 => 'modules/comment/views_plugin_row_comment_rss.inc',
              86 => 'modules/comment/views_plugin_row_comment_view.inc',
              87 => 'modules/contact/views_handler_field_contact_link.inc',
              88 => 'modules/field/views_handler_field_field.inc',
              89 => 'modules/field/views_handler_relationship_entity_reverse.inc',
              90 => 'modules/field/views_handler_argument_field_list.inc',
              91 => 'modules/field/views_handler_filter_field_list_boolean.inc',
              92 => 'modules/field/views_handler_argument_field_list_string.inc',
              93 => 'modules/field/views_handler_filter_field_list.inc',
              94 => 'modules/filter/views_handler_field_filter_format_name.inc',
              95 => 'modules/locale/views_handler_field_node_language.inc',
              96 => 'modules/locale/views_handler_filter_node_language.inc',
              97 => 'modules/locale/views_handler_argument_locale_group.inc',
              98 => 'modules/locale/views_handler_argument_locale_language.inc',
              99 => 'modules/locale/views_handler_field_locale_group.inc',
              100 => 'modules/locale/views_handler_field_locale_language.inc',
              101 => 'modules/locale/views_handler_field_locale_link_edit.inc',
              102 => 'modules/locale/views_handler_filter_locale_group.inc',
              103 => 'modules/locale/views_handler_filter_locale_language.inc',
              104 => 'modules/locale/views_handler_filter_locale_version.inc',
              105 => 'modules/node/views_handler_argument_dates_various.inc',
              106 => 'modules/node/views_handler_argument_node_language.inc',
              107 => 'modules/node/views_handler_argument_node_nid.inc',
              108 => 'modules/node/views_handler_argument_node_type.inc',
              109 => 'modules/node/views_handler_argument_node_vid.inc',
              110 => 'modules/node/views_handler_argument_node_uid_revision.inc',
              111 => 'modules/node/views_handler_field_history_user_timestamp.inc',
              112 => 'modules/node/views_handler_field_node.inc',
              113 => 'modules/node/views_handler_field_node_link.inc',
              114 => 'modules/node/views_handler_field_node_link_delete.inc',
              115 => 'modules/node/views_handler_field_node_link_edit.inc',
              116 => 'modules/node/views_handler_field_node_revision.inc',
              117 => 'modules/node/views_handler_field_node_revision_link.inc',
              118 => 'modules/node/views_handler_field_node_revision_link_delete.inc',
              119 => 'modules/node/views_handler_field_node_revision_link_revert.inc',
              120 => 'modules/node/views_handler_field_node_path.inc',
              121 => 'modules/node/views_handler_field_node_type.inc',
              122 => 'modules/node/views_handler_filter_history_user_timestamp.inc',
              123 => 'modules/node/views_handler_filter_node_access.inc',
              124 => 'modules/node/views_handler_filter_node_status.inc',
              125 => 'modules/node/views_handler_filter_node_type.inc',
              126 => 'modules/node/views_handler_filter_node_uid_revision.inc',
              127 => 'modules/node/views_plugin_argument_default_node.inc',
              128 => 'modules/node/views_plugin_argument_validate_node.inc',
              129 => 'modules/node/views_plugin_row_node_rss.inc',
              130 => 'modules/node/views_plugin_row_node_view.inc',
              131 => 'modules/profile/views_handler_field_profile_date.inc',
              132 => 'modules/profile/views_handler_field_profile_list.inc',
              133 => 'modules/profile/views_handler_filter_profile_selection.inc',
              134 => 'modules/search/views_handler_argument_search.inc',
              135 => 'modules/search/views_handler_field_search_score.inc',
              136 => 'modules/search/views_handler_filter_search.inc',
              137 => 'modules/search/views_handler_sort_search_score.inc',
              138 => 'modules/search/views_plugin_row_search_view.inc',
              139 => 'modules/statistics/views_handler_field_accesslog_path.inc',
              140 => 'modules/system/views_handler_argument_file_fid.inc',
              141 => 'modules/system/views_handler_field_file.inc',
              142 => 'modules/system/views_handler_field_file_extension.inc',
              143 => 'modules/system/views_handler_field_file_filemime.inc',
              144 => 'modules/system/views_handler_field_file_uri.inc',
              145 => 'modules/system/views_handler_field_file_status.inc',
              146 => 'modules/system/views_handler_filter_file_status.inc',
              147 => 'modules/taxonomy/views_handler_argument_taxonomy.inc',
              148 => 'modules/taxonomy/views_handler_argument_term_node_tid.inc',
              149 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth.inc',
              150 => 'modules/taxonomy/views_handler_argument_term_node_tid_depth_modifier.inc',
              151 => 'modules/taxonomy/views_handler_argument_vocabulary_vid.inc',
              152 => 'modules/taxonomy/views_handler_argument_vocabulary_machine_name.inc',
              153 => 'modules/taxonomy/views_handler_field_taxonomy.inc',
              154 => 'modules/taxonomy/views_handler_field_term_node_tid.inc',
              155 => 'modules/taxonomy/views_handler_field_term_link_edit.inc',
              156 => 'modules/taxonomy/views_handler_filter_term_node_tid.inc',
              157 => 'modules/taxonomy/views_handler_filter_term_node_tid_depth.inc',
              158 => 'modules/taxonomy/views_handler_filter_vocabulary_vid.inc',
              159 => 'modules/taxonomy/views_handler_filter_vocabulary_machine_name.inc',
              160 => 'modules/taxonomy/views_handler_relationship_node_term_data.inc',
              161 => 'modules/taxonomy/views_plugin_argument_validate_taxonomy_term.inc',
              162 => 'modules/taxonomy/views_plugin_argument_default_taxonomy_tid.inc',
              163 => 'modules/tracker/views_handler_argument_tracker_comment_user_uid.inc',
              164 => 'modules/tracker/views_handler_filter_tracker_comment_user_uid.inc',
              165 => 'modules/tracker/views_handler_filter_tracker_boolean_operator.inc',
              166 => 'modules/system/views_handler_filter_system_type.inc',
              167 => 'modules/translation/views_handler_argument_node_tnid.inc',
              168 => 'modules/translation/views_handler_field_node_link_translate.inc',
              169 => 'modules/translation/views_handler_field_node_translation_link.inc',
              170 => 'modules/translation/views_handler_filter_node_tnid.inc',
              171 => 'modules/translation/views_handler_filter_node_tnid_child.inc',
              172 => 'modules/translation/views_handler_relationship_translation.inc',
              173 => 'modules/user/views_handler_argument_user_uid.inc',
              174 => 'modules/user/views_handler_argument_users_roles_rid.inc',
              175 => 'modules/user/views_handler_field_user.inc',
              176 => 'modules/user/views_handler_field_user_language.inc',
              177 => 'modules/user/views_handler_field_user_link.inc',
              178 => 'modules/user/views_handler_field_user_link_cancel.inc',
              179 => 'modules/user/views_handler_field_user_link_edit.inc',
              180 => 'modules/user/views_handler_field_user_mail.inc',
              181 => 'modules/user/views_handler_field_user_name.inc',
              182 => 'modules/user/views_handler_field_user_permissions.inc',
              183 => 'modules/user/views_handler_field_user_picture.inc',
              184 => 'modules/user/views_handler_field_user_roles.inc',
              185 => 'modules/user/views_handler_filter_user_current.inc',
              186 => 'modules/user/views_handler_filter_user_name.inc',
              187 => 'modules/user/views_handler_filter_user_permissions.inc',
              188 => 'modules/user/views_handler_filter_user_roles.inc',
              189 => 'modules/user/views_plugin_argument_default_current_user.inc',
              190 => 'modules/user/views_plugin_argument_default_user.inc',
              191 => 'modules/user/views_plugin_argument_validate_user.inc',
              192 => 'modules/user/views_plugin_row_user_view.inc',
              193 => 'plugins/views_plugin_access.inc',
              194 => 'plugins/views_plugin_access_none.inc',
              195 => 'plugins/views_plugin_access_perm.inc',
              196 => 'plugins/views_plugin_access_role.inc',
              197 => 'plugins/views_plugin_argument_default.inc',
              198 => 'plugins/views_plugin_argument_default_php.inc',
              199 => 'plugins/views_plugin_argument_default_fixed.inc',
              200 => 'plugins/views_plugin_argument_default_raw.inc',
              201 => 'plugins/views_plugin_argument_validate.inc',
              202 => 'plugins/views_plugin_argument_validate_numeric.inc',
              203 => 'plugins/views_plugin_argument_validate_php.inc',
              204 => 'plugins/views_plugin_cache.inc',
              205 => 'plugins/views_plugin_cache_none.inc',
              206 => 'plugins/views_plugin_cache_time.inc',
              207 => 'plugins/views_plugin_display.inc',
              208 => 'plugins/views_plugin_display_attachment.inc',
              209 => 'plugins/views_plugin_display_block.inc',
              210 => 'plugins/views_plugin_display_default.inc',
              211 => 'plugins/views_plugin_display_embed.inc',
              212 => 'plugins/views_plugin_display_extender.inc',
              213 => 'plugins/views_plugin_display_feed.inc',
              214 => 'plugins/views_plugin_display_page.inc',
              215 => 'plugins/views_plugin_exposed_form_basic.inc',
              216 => 'plugins/views_plugin_exposed_form.inc',
              217 => 'plugins/views_plugin_exposed_form_input_required.inc',
              218 => 'plugins/views_plugin_localization_core.inc',
              219 => 'plugins/views_plugin_localization.inc',
              220 => 'plugins/views_plugin_localization_none.inc',
              221 => 'plugins/views_plugin_pager.inc',
              222 => 'plugins/views_plugin_pager_full.inc',
              223 => 'plugins/views_plugin_pager_mini.inc',
              224 => 'plugins/views_plugin_pager_none.inc',
              225 => 'plugins/views_plugin_pager_some.inc',
              226 => 'plugins/views_plugin_query.inc',
              227 => 'plugins/views_plugin_query_default.inc',
              228 => 'plugins/views_plugin_row.inc',
              229 => 'plugins/views_plugin_row_fields.inc',
              230 => 'plugins/views_plugin_row_rss_fields.inc',
              231 => 'plugins/views_plugin_style.inc',
              232 => 'plugins/views_plugin_style_default.inc',
              233 => 'plugins/views_plugin_style_grid.inc',
              234 => 'plugins/views_plugin_style_list.inc',
              235 => 'plugins/views_plugin_style_jump_menu.inc',
              236 => 'plugins/views_plugin_style_mapping.inc',
              237 => 'plugins/views_plugin_style_rss.inc',
              238 => 'plugins/views_plugin_style_summary.inc',
              239 => 'plugins/views_plugin_style_summary_jump_menu.inc',
              240 => 'plugins/views_plugin_style_summary_unformatted.inc',
              241 => 'plugins/views_plugin_style_table.inc',
              242 => 'tests/handlers/views_handlers.test',
              243 => 'tests/handlers/views_handler_area_text.test',
              244 => 'tests/handlers/views_handler_argument_null.test',
              245 => 'tests/handlers/views_handler_argument_string.test',
              246 => 'tests/handlers/views_handler_field.test',
              247 => 'tests/handlers/views_handler_field_boolean.test',
              248 => 'tests/handlers/views_handler_field_custom.test',
              249 => 'tests/handlers/views_handler_field_counter.test',
              250 => 'tests/handlers/views_handler_field_date.test',
              251 => 'tests/handlers/views_handler_field_file_extension.test',
              252 => 'tests/handlers/views_handler_field_file_size.test',
              253 => 'tests/handlers/views_handler_field_math.test',
              254 => 'tests/handlers/views_handler_field_url.test',
              255 => 'tests/handlers/views_handler_field_xss.test',
              256 => 'tests/handlers/views_handler_filter_combine.test',
              257 => 'tests/handlers/views_handler_filter_date.test',
              258 => 'tests/handlers/views_handler_filter_equality.test',
              259 => 'tests/handlers/views_handler_filter_in_operator.test',
              260 => 'tests/handlers/views_handler_filter_numeric.test',
              261 => 'tests/handlers/views_handler_filter_string.test',
              262 => 'tests/handlers/views_handler_sort_random.test',
              263 => 'tests/handlers/views_handler_sort_date.test',
              264 => 'tests/handlers/views_handler_sort.test',
              265 => 'tests/test_handlers/views_test_area_access.inc',
              266 => 'tests/test_plugins/views_test_plugin_access_test_dynamic.inc',
              267 => 'tests/test_plugins/views_test_plugin_access_test_static.inc',
              268 => 'tests/test_plugins/views_test_plugin_style_test_mapping.inc',
              269 => 'tests/plugins/views_plugin_display.test',
              270 => 'tests/styles/views_plugin_style_jump_menu.test',
              271 => 'tests/styles/views_plugin_style.test',
              272 => 'tests/styles/views_plugin_style_base.test',
              273 => 'tests/styles/views_plugin_style_mapping.test',
              274 => 'tests/styles/views_plugin_style_unformatted.test',
              275 => 'tests/views_access.test',
              276 => 'tests/views_analyze.test',
              277 => 'tests/views_basic.test',
              278 => 'tests/views_argument_default.test',
              279 => 'tests/views_argument_validator.test',
              280 => 'tests/views_exposed_form.test',
              281 => 'tests/field/views_fieldapi.test',
              282 => 'tests/views_glossary.test',
              283 => 'tests/views_groupby.test',
              284 => 'tests/views_handlers.test',
              285 => 'tests/views_module.test',
              286 => 'tests/views_pager.test',
              287 => 'tests/views_plugin_localization_test.inc',
              288 => 'tests/views_translatable.test',
              289 => 'tests/views_query.test',
              290 => 'tests/views_upgrade.test',
              291 => 'tests/views_test.views_default.inc',
              292 => 'tests/comment/views_handler_argument_comment_user_uid.test',
              293 => 'tests/comment/views_handler_filter_comment_user_uid.test',
              294 => 'tests/node/views_node_revision_relations.test',
              295 => 'tests/taxonomy/views_handler_relationship_node_term_data.test',
              296 => 'tests/user/views_handler_field_user_name.test',
              297 => 'tests/user/views_user_argument_default.test',
              298 => 'tests/user/views_user_argument_validate.test',
              299 => 'tests/user/views_user.test',
              300 => 'tests/views_cache.test',
              301 => 'tests/views_view.test',
              302 => 'tests/views_ui.test',
            ),
            'version' => '7.x-3.13',
            'project' => 'views',
            'datestamp' => '1446804876',
          ),
          'schema_version' => '7302',
          'version' => '7.x-3.13',
        ),
        'jquery_colorpicker' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/jquery_colorpicker/jquery_colorpicker.module',
          'basename' => 'jquery_colorpicker.module',
          'name' => 'jquery_colorpicker',
          'info' => 
          array (
            'name' => 'Jquery Colorpicker',
            'description' => 'Creates Form API jquery colorpicker element',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'libraries',
            ),
            'version' => '7.x-1.2',
            'project' => 'jquery_colorpicker',
            'datestamp' => '1442039944',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.2',
        ),
        'views_bulk_operations' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/views_bulk_operations/views_bulk_operations.module',
          'basename' => 'views_bulk_operations.module',
          'name' => 'views_bulk_operations',
          'info' => 
          array (
            'name' => 'Views Bulk Operations',
            'description' => 'Provides a way of selecting multiple rows and applying operations to them.',
            'dependencies' => 
            array (
              0 => 'entity',
              1 => 'views',
            ),
            'package' => 'Views',
            'core' => '7.x',
            'php' => '5.2.9',
            'files' => 
            array (
              0 => 'plugins/operation_types/base.class.php',
              1 => 'views/views_bulk_operations_handler_field_operations.inc',
            ),
            'version' => '7.x-3.3',
            'project' => 'views_bulk_operations',
            'datestamp' => '1435764542',
          ),
          'schema_version' => 0,
          'version' => '7.x-3.3',
        ),
        'actions_permissions' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/views_bulk_operations/actions_permissions.module',
          'basename' => 'actions_permissions.module',
          'name' => 'actions_permissions',
          'info' => 
          array (
            'name' => 'Actions permissions (VBO)',
            'description' => 'Provides permission-based access control for actions. Used by Views Bulk Operations.',
            'package' => 'Administration',
            'core' => '7.x',
            'version' => '7.x-3.3',
            'project' => 'views_bulk_operations',
            'datestamp' => '1435764542',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-3.3',
        ),
        'stylizer' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/stylizer/stylizer.module',
          'basename' => 'stylizer.module',
          'name' => 'stylizer',
          'info' => 
          array (
            'name' => 'Stylizer',
            'description' => 'Create custom styles for applications such as Panels.',
            'core' => '7.x',
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'dependencies' => 
            array (
              0 => 'ctools',
              1 => 'color',
            ),
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'ctools_custom_content' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/ctools_custom_content/ctools_custom_content.module',
          'basename' => 'ctools_custom_content.module',
          'name' => 'ctools_custom_content',
          'info' => 
          array (
            'name' => 'Custom content panes',
            'description' => 'Create custom, exportable, reusable content panes for applications like Panels.',
            'core' => '7.x',
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'ctools_ajax_sample' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/ctools_ajax_sample/ctools_ajax_sample.module',
          'basename' => 'ctools_ajax_sample.module',
          'name' => 'ctools_ajax_sample',
          'info' => 
          array (
            'name' => 'Chaos Tools (CTools) AJAX Example',
            'description' => 'Shows how to use the power of Chaos AJAX.',
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'core' => '7.x',
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'ctools_access_ruleset' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/ctools_access_ruleset/ctools_access_ruleset.module',
          'basename' => 'ctools_access_ruleset.module',
          'name' => 'ctools_access_ruleset',
          'info' => 
          array (
            'name' => 'Custom rulesets',
            'description' => 'Create custom, exportable, reusable access rulesets for applications like Panels.',
            'core' => '7.x',
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'views_content' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/views_content/views_content.module',
          'basename' => 'views_content.module',
          'name' => 'views_content',
          'info' => 
          array (
            'name' => 'Views content panes',
            'description' => 'Allows Views content to be used in Panels, Dashboard and other modules which use the CTools Content API.',
            'package' => 'Chaos tool suite',
            'dependencies' => 
            array (
              0 => 'ctools',
              1 => 'views',
            ),
            'core' => '7.x',
            'version' => '7.x-1.9',
            'files' => 
            array (
              0 => 'plugins/views/views_content_plugin_display_ctools_context.inc',
              1 => 'plugins/views/views_content_plugin_display_panel_pane.inc',
              2 => 'plugins/views/views_content_plugin_style_ctools_context.inc',
            ),
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'page_manager' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/page_manager/page_manager.module',
          'basename' => 'page_manager.module',
          'name' => 'page_manager',
          'info' => 
          array (
            'name' => 'Page manager',
            'description' => 'Provides a UI and API to manage pages within the site.',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'bulk_export' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/bulk_export/bulk_export.module',
          'basename' => 'bulk_export.module',
          'name' => 'bulk_export',
          'info' => 
          array (
            'name' => 'Bulk Export',
            'description' => 'Performs bulk exporting of data objects known about by Chaos tools.',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'ctools_plugin_example' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/ctools_plugin_example/ctools_plugin_example.module',
          'basename' => 'ctools_plugin_example.module',
          'name' => 'ctools_plugin_example',
          'info' => 
          array (
            'name' => 'Chaos Tools (CTools) Plugin Example',
            'description' => 'Shows how an external module can provide ctools plugins (for Panels, etc.).',
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'dependencies' => 
            array (
              0 => 'ctools',
              1 => 'panels',
              2 => 'page_manager',
              3 => 'advanced_help',
            ),
            'core' => '7.x',
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'term_depth' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/term_depth/term_depth.module',
          'basename' => 'term_depth.module',
          'name' => 'term_depth',
          'info' => 
          array (
            'name' => 'Term Depth access',
            'description' => 'Controls access to context based upon term depth',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'ctools',
            ),
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.9',
        ),
        'ctools' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/ctools/ctools.module',
          'basename' => 'ctools.module',
          'name' => 'ctools',
          'info' => 
          array (
            'name' => 'Chaos tools',
            'description' => 'A library of helpful tools by Merlin of Chaos.',
            'core' => '7.x',
            'package' => 'Chaos tool suite',
            'version' => '7.x-1.9',
            'files' => 
            array (
              0 => 'includes/context.inc',
              1 => 'includes/css-cache.inc',
              2 => 'includes/math-expr.inc',
              3 => 'includes/stylizer.inc',
              4 => 'tests/css_cache.test',
            ),
            'project' => 'ctools',
            'datestamp' => '1440020680',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7001',
          'version' => '7.x-1.9',
        ),
        'features' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/features/features.module',
          'basename' => 'features.module',
          'name' => 'features',
          'info' => 
          array (
            'name' => 'Features',
            'description' => 'Provides feature management for Drupal.',
            'core' => '7.x',
            'package' => 'Features',
            'files' => 
            array (
              0 => 'tests/features.test',
            ),
            'configure' => 'admin/structure/features/settings',
            'version' => '7.x-2.7',
            'project' => 'features',
            'datestamp' => '1444829630',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7200',
          'version' => '7.x-2.7',
        ),
        'jquery_update' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/contrib/jquery_update/jquery_update.module',
          'basename' => 'jquery_update.module',
          'name' => 'jquery_update',
          'info' => 
          array (
            'name' => 'jQuery Update',
            'description' => 'Update jQuery and jQuery UI to a more recent version.',
            'package' => 'User interface',
            'core' => '7.x',
            'configure' => 'admin/config/development/jquery_update',
            'version' => '7.x-3.0-alpha3',
            'project' => 'jquery_update',
            'datestamp' => '1445382241',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7001',
          'version' => '7.x-3.0-alpha3',
        ),
        'masquerade' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/masquerade/masquerade.module',
          'basename' => 'masquerade.module',
          'name' => 'masquerade',
          'info' => 
          array (
            'name' => 'Masquerade',
            'description' => 'This module allows permitted users to masquerade as other users.',
            'core' => '7.x',
            'files' => 
            array (
              0 => 'masquerade.test',
            ),
            'configure' => 'admin/config/people/masquerade',
            'version' => '7.x-1.0-rc7',
            'project' => 'masquerade',
            'datestamp' => '1394333639',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7001',
          'version' => '7.x-1.0-rc7',
        ),
        'admin_menu_toolbar' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/admin_menu/admin_menu_toolbar/admin_menu_toolbar.module',
          'basename' => 'admin_menu_toolbar.module',
          'name' => 'admin_menu_toolbar',
          'info' => 
          array (
            'name' => 'Administration menu Toolbar style',
            'description' => 'A better Toolbar.',
            'package' => 'Administration',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'admin_menu',
            ),
            'version' => '7.x-3.0-rc5',
            'project' => 'admin_menu',
            'datestamp' => '1419029284',
            'php' => '5.2.4',
          ),
          'schema_version' => '6300',
          'version' => '7.x-3.0-rc5',
        ),
        'admin_devel' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/admin_menu/admin_devel/admin_devel.module',
          'basename' => 'admin_devel.module',
          'name' => 'admin_devel',
          'info' => 
          array (
            'name' => 'Administration Development tools',
            'description' => 'Administration and debugging functionality for developers and site builders.',
            'package' => 'Administration',
            'core' => '7.x',
            'scripts' => 
            array (
              0 => 'admin_devel.js',
            ),
            'version' => '7.x-3.0-rc5',
            'project' => 'admin_menu',
            'datestamp' => '1419029284',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-3.0-rc5',
        ),
        'admin_menu' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/admin_menu/admin_menu.module',
          'basename' => 'admin_menu.module',
          'name' => 'admin_menu',
          'info' => 
          array (
            'name' => 'Administration menu',
            'description' => 'Provides a dropdown menu to most administrative tasks and other common destinations (to users with the proper permissions).',
            'package' => 'Administration',
            'core' => '7.x',
            'configure' => 'admin/config/administration/admin_menu',
            'dependencies' => 
            array (
              0 => 'system (>7.10)',
            ),
            'files' => 
            array (
              0 => 'tests/admin_menu.test',
            ),
            'version' => '7.x-3.0-rc5',
            'project' => 'admin_menu',
            'datestamp' => '1419029284',
            'php' => '5.2.4',
          ),
          'schema_version' => '7304',
          'version' => '7.x-3.0-rc5',
        ),
        'admin_menu_environmental' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/admin_menu_environmental/admin_menu_environmental.module',
          'basename' => 'admin_menu_environmental.module',
          'name' => 'admin_menu_environmental',
          'info' => 
          array (
            'name' => 'Administration menu environmental',
            'description' => 'Change the admin menu\'s color according to environment and integration with masquerade.',
            'dependencies' => 
            array (
              0 => 'admin_menu',
              1 => 'masquerade',
            ),
            'package' => 'Administration',
            'configure' => 'admin/config/administration/admin_menu/environmental',
            'core' => '7.x',
            'version' => '7.x-1.x-dev',
            'project' => 'admin_menu_environmental',
            'datestamp' => '1430301482',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.x-dev',
        ),
        'masquerade_drush' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/masquerade_extras/masquerade_drush/masquerade_drush.module',
          'basename' => 'masquerade_drush.module',
          'name' => 'masquerade_drush',
          'info' => 
          array (
            'name' => 'Masquerade Drush Commands',
            'core' => '7.x',
            'description' => 'Provides Drush commands for masquerade.',
            'version' => '7.x-2.0-beta4',
            'project' => 'masquerade_extras',
            'datestamp' => '1343339861',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.0-beta4',
        ),
        'masquerade_context' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/masquerade_extras/masquerade_context/masquerade_context.module',
          'basename' => 'masquerade_context.module',
          'name' => 'masquerade_context',
          'info' => 
          array (
            'name' => 'Context support for Masquerade',
            'package' => 'Masquerade Extras',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'masquerade',
              1 => 'context',
            ),
            'version' => '7.x-2.0-beta4',
            'project' => 'masquerade_extras',
            'datestamp' => '1343339861',
            'description' => '',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.0-beta4',
        ),
        'masquerade_ctools' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/masquerade_extras/masquerade_ctools/masquerade_ctools.module',
          'basename' => 'masquerade_ctools.module',
          'name' => 'masquerade_ctools',
          'info' => 
          array (
            'name' => 'CTools Plugins for Masquerade',
            'package' => 'Masquerade Extras',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'masquerade',
              1 => 'ctools',
            ),
            'version' => '7.x-2.0-beta4',
            'project' => 'masquerade_extras',
            'datestamp' => '1343339861',
            'description' => '',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.0-beta4',
        ),
        'masquerade_views' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/masquerade_extras/masquerade_views/masquerade_views.module',
          'basename' => 'masquerade_views.module',
          'name' => 'masquerade_views',
          'info' => 
          array (
            'name' => 'Views support for Masquerade',
            'package' => 'Masquerade Extras',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'masquerade',
              1 => 'views',
            ),
            'files' => 
            array (
              0 => 'handlers/masquerade_handler_field_user.inc',
            ),
            'version' => '7.x-2.0-beta4',
            'project' => 'masquerade_extras',
            'datestamp' => '1343339861',
            'description' => '',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-2.0-beta4',
        ),
        'module_filter' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/module_filter/module_filter.module',
          'basename' => 'module_filter.module',
          'name' => 'module_filter',
          'info' => 
          array (
            'name' => 'Module filter',
            'description' => 'Filter the modules list.',
            'core' => '7.x',
            'package' => 'Administration',
            'files' => 
            array (
              0 => 'module_filter.install',
              1 => 'module_filter.js',
              2 => 'module_filter.module',
              3 => 'module_filter.admin.inc',
              4 => 'module_filter.theme.inc',
              5 => 'css/module_filter.css',
              6 => 'css/module_filter_tab.css',
              7 => 'js/module_filter.js',
              8 => 'js/module_filter_tab.js',
            ),
            'configure' => 'admin/config/user-interface/modulefilter',
            'version' => '7.x-2.0',
            'project' => 'module_filter',
            'datestamp' => '1424631189',
            'dependencies' => 
            array (
            ),
            'php' => '5.2.4',
          ),
          'schema_version' => '7201',
          'version' => '7.x-2.0',
        ),
        'admin_views' => 
        array (
          'filename' => '/var/aegir/platforms/development-7.41/profiles/development/modules/admin/admin_views/admin_views.module',
          'basename' => 'admin_views.module',
          'name' => 'admin_views',
          'info' => 
          array (
            'name' => 'Administration views',
            'description' => 'Replaces all system object management pages in Drupal core with real views.',
            'package' => 'Administration',
            'core' => '7.x',
            'dependencies' => 
            array (
              0 => 'views',
              1 => 'views_bulk_operations (>=7.x-3.2)',
            ),
            'files' => 
            array (
              0 => 'plugins/views_plugin_display_system.inc',
              1 => 'plugins/views_plugin_access_menu.inc',
              2 => 'tests/admin_views.test',
            ),
            'version' => '7.x-1.5',
            'project' => 'admin_views',
            'datestamp' => '1436376676',
            'php' => '5.2.4',
          ),
          'schema_version' => 0,
          'version' => '7.x-1.5',
        ),
      ),
      'themes' => 
      array (
      ),
    ),
  ),
);